# MASTI - Messenger Application for Student-Teacher Interaction

## Teams

1. Networking
2. Image Processing
3. Messaging
4. Schema
5. UI/UX
6. Testing & Validation
7. Persistence

## Functionalities of Modules

### Networking

1. Provide an API to send data to a specific IP and Port.
2. Provide an API where any module can subscribe to listen to specific events.
3. Every instance of the Networking object should create a new event handler.
4. Sending a Message
   - Use queues to store messages and try sending them accordingly (try multiple times).
   - If a message is sent dispatch an event accordingly (depending on who called it).
   - One way to achieve the above is to take an event as the second argument.
   - Dispatch the event with a structured message. (The respective team can then proceed by unpacking the message)
5. Receiveing a Message
   - Keep listening on a specific Port.
   - There can be only one application listening on a specific Port at any given time.
   - The Networking team should provide a singleton to work with.
   - Any module should be able to subscribe to that singleton with a specific tag.
   - Depending on the tag dispatch the specific event.
6. Every instantiation should return the same object (singleton).
7. Please use the conventions given for event naming (provided later).

### Image Processing

1. Subscribe to the object instantiated from Networking module.
2. You will need handle the events you want to listen to.
3. Provide an API where any module can subscribe to specific events.
4. You cannot directly link the subscribers to the Networking instance. Create your own Publisher.
5. Some possible events are `receivedScreen`, `receivedImage`, `errorSendScreen`, `errorScreenCapture`, `notificationStartedScreenCapture`, `notificationStoppedScreenCapture` etc.
6. Provide an API to start/stop screen capture.
7. Talk to the Schema team to decide the structure of the message.
8. Provide an API to retreive/store an image with specific parameters (username, sessionId etc.).

### Messaging

1. Subscribe to the object instantiated from Networking module.
2. You will need to handle the events you are interested in.
3. Provide an API where any module can subscribe to specific events.
4. You cannot directly link the subscribers to the Networking instance. Create your own Publisher.
5. Some possible events are `successText`, `failText`, `receivedText` etc.
6. Talk to the Schema team to decide the structure of the message.
7. Provide an API to retreive/store messages with specific parameters (username, sessionId etc.).
8. When storing the messages modify the messages accordingly (add required parameters) and then store.
9. Give an API for deleting messages until a specific timestamp.
10. Give an API for saving specific sessions to a file. For this, you need to use the Persistence API.

### Schema

1. Provide an API to encode and decode data for Image processing team and Text processing team (JSON preferably).
2. Make the message structure based on the requirements of the Text processing and Image processing team.

### UI/UX

1. You are free to design the display as you wish (I would suggest you to make the design in the end).
2. Instantiate objects from Text processing and Image processing.
3. You will need to subscribe and handle events dispached from the Text processing and Image processing publishers.
4. No Auth required. Keep it simple.
5. Use thread-safe data structures.
6. Need to create 2 modules, client and server.
7. Run a deletion mechanism on startup, provided by the Messaging team.
8. On start up, request a username.
9. Create a toggle to check if user wants to store messages.
10. If yes, maintain another list and add id's of messages to that list (somehow differentiate the messages you want to save and the ones you want to discard).
11. On closing the application (or clicking a button), filter the main list for the items to be saved and call the save function provided by Messaging module.
12. Provide a set of UI elements which function to save messages to a file (admin).

### Persistence

1. Provide API to store and receive data.
2. Your input will be a dictionary of key and values. Save accordingly.
3. All the data required should be in the dictionary provided.
4. You can request a few fields specifically.
5. Provide a way to extract content from the database and save it in a file, given specific parameters.

### Tools and Validation

1. Provide an API to log data.
2. Provide an API to retrieve logs.
3. Create a Telemetry class to compute statictics.
4. Create a way to run all the test files in the module.

## Additional Notes
1. Distinguish users by their username.

## High Level Module Diagram

![Module Diagram](./README/moduleDiagram.png)

## Workflow

### Sending a Message

![Sending Message](./README/sendMessage.png)

1. The UI / UX module will call the specific `send` function provided by the Messaging module.
2. The Messaging will use the schema module to encode the message.
3. The Messaging module recieves the encoded message.
4. The Mesaging module calls the function provided by the networking module.
5. The Networking module dispatches a `Messaging` event depending on the status of the message sent.
6. The Messaging module then dispatches and event depending on the event dispatched by the Networking module. The UI / UX reacts to the event accordingly

### Receiveing a Message

![Receiving Message](./README/receiveMessage.png)

1. When the networking module recieves some data with the `text` tag, it dispatches the appropriate event.
2. Since the Messaging module is subscribed to the Networking module, It gets the message. It then uses the Schema API to decode the data.
3. The Messaging module gets the decoded data.
4. The Messaging module dispatches the apropriate event. The UI / UX reacts accordingly.

### Storing Messages

![Storing Message](./README/storeMessages.png)

1. The Messaging module calls the Persistence module API for storing the Messages. It needs to also submit a pointer to the list of messages to save.
2. The Messaging module adds the required session tags to each message and uses the persistece API to save the Messages.

### Retrieveing Messages

![Retrieveing Message](./README/retrieveMessages.png)

1. The UI / UX requests the Messaging module with the specific session id and other parameters.
2. The Messaging module calls the Persistence API.
3. The Persistence diapatches the appropriate event with the data required.
4. The Messaging module sanitizes the data, removing the unwanted tags and dispatched an event. The UI / UX updates accordinngly.

### Sending an Image

![Sending Image](./README/sendImage.png)

_After the UI / UX gives the permission to capture screen._

1. The Image Processing module captures the screen and calls the Schema API to encode the Image.
2. The Image Processing module recieves the encoded message (here message is the image message) (Confusing terminology).
3. The Image Processing module calls the function provided by the networking module.
4. The Networking module dispatches an `ImageProcessing` event depending on the status of the message sent.
5. The Image Processing module then dispatches and event depending on the event dispatched by the Networking module. The UI / UX reacts to the event accordingly

### Receiveing an Image

![Receiving Image](./README/receiveImage.png)

1. When the networking module recieves some data with the `screen` tag, it dispatches the appropriate event.
2. Since the Image Processing module is subscribed to the Networking module, It gets the message. It then uses the Schema API to decode the data.
3. The Image Processing module gets the decoded Image.
4. The Image Processing module dispatches the apropriate event. The UI / UX reacts accordingly.

### Storing the Image

![Storing Image](./README/storeImage.png)

_The user can choose so save a specific screen for the session._
_Only 1 screen per session_

1. The Image Processing module calls the Persistence module API for storing the current image (screen). It needs to also submit a pointer to the Image to save.
2. The Image Processing module adds the required session tags to the Image and uses the persistece API to save the image.

### Retrieveing the Image

![Retrieveing Image](./README/retrieveImage.png)

1. The UI / UX requests the Image Processing module with the specific session id and other parameters.
2. The Image Processing module calls the Persistence API.
3. The Persistence diapatches the appropriate event with the data required.
4. The Image Processing module sanitizes the data, removing the unwanted tags and dispatched an event. The UI / UX updates accordinngly.

### Saving Messages to a File

1. The UI calls the specific function in the Messaging module.
2. The Messaging module calls the specific functions in the Persistence module.
3. The Persistence module does the work and dispatches an event.
4. The Messaging module responds to the event by dispatching another event, to which the UI / UX responds accordingly.

