# Persistence Team

## Members
1. Prabal Vashisht (Leader/Designer/Coder)


## Objectives
* Provide an API for storing/retrieving chat history.
## Dependencies 
The following components depend on my module. 

* Messaging/Image Processing  module 


My module depends on no module.

## Activity flow

![User sends a message](Readme/flow_send.png)

FIG. When text is sent by user and it is being recorded (activity flows in accordance to the numbering).

![User receives a message](Readme/flow_receive.png)

FIG. When text is received by user and it is being recorded (activity flows in accordance to the numbering)

## Class Diagram
![Class Diagram](path/to/the/image.jpg)

##Design decisions

* **MongoDB Vs MySQL:** For storing data on disk, the team has voted on to using MongoDB since it is faster if there are less relationships between entities, if compared to MySQL. Moreover the schema team has chosen JSON format to encode the data, which can directly be stored using MongoDB.

* **Automatically recording Vs Specifying when to record:** In one session, the user can specify when to start recording the message (i.e. storing permanently) and when to stop (i.e. it won't be stored permanently). There will be a toggle button on the UI for this purpose.

* **Deletion of data:** The Persistence API will provide a method to the Messaging team to delete the old data from the database.


## Interface 

Storage
```
public interface IStorage
{
	void store (String str, Timestamp time);
	ArrayList<Dictionary> retrieve (Id id, Timestamp startTime, Timestamp endTime );
	void delete (Timestamp lastTime);
	
}

```
## Work Distribution
One member team, hence Prabal Vashisht will be accomplishing the required tasks.
