# Tools and Validation
_Prepared by Anish M M, 111501006 on 09-09-2018_
## Team Members
1. Adrian McDonald Tariang
1. Anish M M

### Test Harness

#### Logging
 - The following interface will be provided for logger.
 
 
```
     
		public interface ILogger
        
		{
		    void LogInfo(string message);
		    void LogWarning(string message);
		    void LogError(string message);
            
		    // Send the logs to a prespecified mail id.
		    void MailLog();   
		}
        
```
    

- Give two implementations for `ILogger` : 
	- Logging to file `FileLogger`
	- Logging to console `ConsoleLogger`
- Create a class factory `LoggerFactory` that creates and returns appropriate logger instance.
	- By default, it creates a `FileLogger`. 
		- Reason: The same logger is used for Diagnostics and there, we wouldn't want to print the logs to the console but to a file, by default.
	- Can make it choose `ConsoleLogger` by passing a flag to the constructor of the factory.
		- Since this will be the usual scenario while testing, we will implicitly do this while calling the factory, from the test harness executive. 
		- Only if during testing, the developer wants to write to a file, do they have to add a flag in the command. (Will be mentioned later.)
		- Verbosity of logs can be controlled through command line flags. By default, set to least verbose (only log errors) since this will be the case when logging during Diagnostics. (Flags mentioned later.)

#### Testing
- The following interface will be provided for testing.

	
```

    
	    public interface ITest
	    {
	        bool Run();   
	    }
    
```
    
- Each module will implement `ITest` interface to create their tests. Write the test inside the `run()` method.
- The tests can use the above given logging options within their project. For this they need a logger object. Testing is done sequentially. So, there is no need to think about multiple objects, singleton, etc.. One object can be created by the test harness executive (using `LoggerFactory`)and passed on to the Test objects through constructor.
-  Each module will name their test(s) using the following convention : The class will be named as `<unit of work>_<state under test>_<expected behavior>_Test` (assuming a class for a test). If instead, you plan to group a set of related tests into one class, you can name the class as `<unit of work>_<common feature being tested>_Test` and do the multiple tests inside the `run()` method, take conjunction of the results and return that. You may name these sub-methods using the above convention `<unit of work>_<state under test>_<expected behavior>` . 
	- Example :
    
    
    
```  
	// One test in one class.
	class Sum_NegativeNumberAs1stParam_ExceptionThrown_Test : ITest
	{...}	

	// Multiple related tests in a class.
	class Sum_ParamTypeErrors_Test : ITest
	{...}
        
```


	- For more details, check out [Roy Osherove's naming strategy](http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html). 'Test' has been appended to his prescribed strategy to make identifying tests easy.
- Running tests :
	- run tests individually by name.
	- run all tests in a directory.
		- to identify all tests, the contents of the current directory will be parsed (recursively) and files ending with the string '**_Test**' will be used, in accordance with above given naming convention.
- While running the tests, in addition to logs, details regarding how many tests passed, how many failed, and the list of failed test names will be printed/saved, as is the case.
- This log for test statistics will contain time stamp.

#### Commands and flags
- Run a single test 
```
	     
         
	     MastiCLI -t Sum_ParamTypeErrors_Test
         
```     
     
- Run multiple tests

```
        MastiCLI -t all
```

 - Log verbosity 
	 - `-v `         _only errors, default_
	 -  `-vv`        _errors, warnings_
	 -  `-vvv`      _all_
 
 
### Class Diagram for Test Harness
 
 ![class diagram](test_harness_class_diagram.png)
 
### Work distribution
- Adrian
	- Test Harness
	- Command line tool
- Anish
	- Diagnostics
	- Telemetry
 
