# Tools and Validation Team
_Prepared by Adrian McDonald Tariang, 111501001 on 10-09-2018_

## Members
1. Adrian McDonald Tariang
2. Anish M M

## Objectives
* Create a CommandLine tool to execute Tests and Tools for the project
* Provide a Test Harness for Unit and Integration Testing in module development
* Develop Loggers to print to Console or File for the Tests
* Diagnostics feature for the module developers
* Collect Telemetry and Usage Statistics during program execution

## Dependencies 
All modules depend on Tools and Validation

The Test Harness is a stand-alone console application, while the Diagnostics and the Telemetry would need to be initialised or started by the first module running the MASTI software.

## Class Diagram
_Diagram prepared by Anish M M, 111501006 on 09-09-2018_

![Test Harness](Specs/test_harness_class_diagram.png)

## Interface 
Logger
```
	public interface ILogger
    
	{
	    void LogInfo(string message);
	    void LogWarning(string message);
	    void LogError(string message);
        
	    // Send the logs to a prespecified mail id.
	    void MailLog();   
	}
        
```

Test
```
    public interface ITest
    {
        bool Run();   
    }
```

Telemetry
```
	public interface ITelemetry
    {
    	// Hold the data that needs to be collected by the module
    	IDictionary<string, object> dataCapture;

    	// Extract telemetry and statistics from new data
        void Calculate(object data);

        // Summarise captured data and prepare for telemetry storage
        void Summarise();
    }
```

## Work Distribution
- Adrian
	- Test Harness
	- Command line tool
- Anish
	- Diagnostics
	- Telemetry