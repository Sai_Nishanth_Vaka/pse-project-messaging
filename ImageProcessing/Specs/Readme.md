# ImageProcessing Team

The Image Processing Team develops the module that enables the clients to share their screen with the server.

## Members

|||
|--------------------|:--------------:|
| Amish Ranjan       | Code           |
| Axel James         | Design         |
| Ravindra Kumar     | Compression    |
| Sooraj Tom         | Team Lead      |
| SST Siddhardha     | Compression    |
| Suman Saurav Panda | Code           |

## Objectives

1. Use Networking API & Schema API, send screen to the server IP after encoding it . (Client)
2. Use Networking API & Schema API, recieve screen data from client, decode it and publish it to UI. (Server)
3. Provide an API to start and stop the sharing from UI. The server sends signal for starting the screen  sharing.
4. Capture the screen from client and apply compression if possible.

## Dependencies 
The following components depend on this module.

* UI

This module depends on the following modules.

* Networking
* Schema

## Module Diagram
![Module Diagram](Readme/ModuleDiagram.png)

## Class Diagram
`To be updated after meeting`

## Activity Diagram
![Activity Diagram](path/to/image.png)


## Interface 
```
interface IImageProcessing
{
    void StartSharing(String clientIp);
    void StopSharing();
}
```

## Internal Components
### Component 1:   
Describe. 
### Component 2:  
Describe

## Work Distribution

`Detailed work distribution to be added after next team meeting.`
